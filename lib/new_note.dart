import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter01/variables.dart';

class NewNote extends StatefulWidget {
  const NewNote({Key? key}) : super(key: key);

  @override
  _NewNoteState createState() => _NewNoteState();
}

class _NewNoteState extends State<NewNote> {
  final _c = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.95,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  maxLength: 1000,
                  maxLines: (MediaQuery
                      .of(context)
                      .size
                      .height * 0.8).round(),
                  controller: _c,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: const InputDecoration(
                    labelText: "Enter Your Note",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.red
                      ),
                      onPressed: () {
                        _c.clear();
                      },
                      child: const Icon(Icons.delete_forever)),
                  ElevatedButton(
                      onPressed: () {
                        notes.add(_c.text);
                        _c.clear();
                        Navigator.pop(context);
                      },
                      child: const Text("SAVE"))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
