import 'package:flutter/material.dart';
import 'package:flutter01/variables.dart';

import 'new_note.dart';

void main() {
  runApp(TodoApp());
}

class TodoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => const NewNote())
          );
        },
        child: const Icon(Icons.add),
      ),
      appBar: AppBar(
        title: Text('To-do List'),
      ),
      body: SafeArea(
        child: StreamBuilder(
          initialData: notes.length,
          builder: (context,snapshot){
            if(notes.isEmpty){
              return const Text("You Have no texts");
            }else{
              return ListView.builder(
                itemCount: notes.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(notes[index]),
                  );
                },
              );
            }
          },
        )
      ),
    );
  }
}






